import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('D:\\Programs\\Android-MyDemoAppRN.1.2.0.build-231.apk', true)

'Get Device Height and Store in device_height variable'
device_Height = Mobile.getDeviceHeight()

'Get Width Height and Store in device_Width variable'
device_Width = Mobile.getDeviceWidth()

'Storing the startX value by dividing device width by 2. Because x coordinates are constant for Vertical Swiping'
int startX = device_Width / 2

'Here startX and endX values are equal for vertical Swiping for that assigning startX value to endX'
int endX = startX

'Storing the startY value'
int startY = device_Height * 0.30

'Storing the endY value'
int endY = device_Height * 0.70

Mobile.tap(findTestObject('Object Repository/TC06/android.widget.ImageView'), 0)

Mobile.tap(findTestObject('Object Repository/TC06/android.view.ViewGroup'), 0)

Mobile.tap(findTestObject('Object Repository/TC06/android.widget.ImageView (1)'), 0)

Mobile.tap(findTestObject('Object Repository/TC06/android.view.ViewGroup (1)'), 0)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText'), 'bob@example.com', 0)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText (1)'), '10203040', 0)

Mobile.tap(findTestObject('Object Repository/TC06/android.view.ViewGroup (2)'), 0)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText - Rebecca Winter'), 'Rebecca Winter', 0)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText - Mandorley 112'), 'Mandorley 112', 0)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText - Entrance 1'), 'Entrance 1', 0)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText - Truro'), 'Truro', 0)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText - Cornwall (1)'), 'Cornwall', 0)

'Swipe Vertical from top to bottom'
Mobile.swipe(startX, endY, endX, startY)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText - 89750'), '89750', 0)

'Swipe Vertical from top to bottom'
Mobile.swipe(startX, endY, endX, startY)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText - United Kingdom'), 'United Kingdom', 0)

Mobile.tap(findTestObject('Object Repository/TC06/android.view.ViewGroup (4)'), 0)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText - Rebecca Winter (1)'), 'Rebecca Winter', 
    0)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText - 3258 1265 7568 789'), '3258 1265 7568 789', 
    0)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText - 0325'), '03/25', 0)

Mobile.setText(findTestObject('Object Repository/TC06/android.widget.EditText - 123'), '123', 0)

Mobile.tap(findTestObject('Object Repository/TC06/android.view.ViewGroup (5)'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/TC06/android.widget.TextView - Checkout'), 0)

Mobile.tap(findTestObject('Object Repository/TC06/android.view.ViewGroup (6)'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/TC06/android.widget.TextView - Checkout Complete'), 0)

Mobile.closeApplication()

